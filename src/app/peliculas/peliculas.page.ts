import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-peliculas',
  templateUrl: './peliculas.page.html',
  styleUrls: ['./peliculas.page.scss'],
})
export class PeliculasPage implements OnInit {

  token = localStorage.getItem("token");
  peliculas: any = [];


  constructor(private router: Router,
    private http: HttpClient) { }

  ngOnInit() {
    console.log("token", this.token);
    this.getPeliculas().subscribe(res=>{
      console.log("Res",res)
      this.peliculas = res;
    });
  }

  goToHome(){
    this.router.navigate(['/home'])
  }

  getPeliculas(){
      return this.http
      .get("assets/files/peliculas.json")
      .pipe(
        map((res:any) =>{
          return res.data;
        })
      )
  }

}
