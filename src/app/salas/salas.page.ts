import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { map } from 'rxjs/operators';

@Component({
  selector: 'app-salas',
  templateUrl: './salas.page.html',
  styleUrls: ['./salas.page.scss'],
})
export class SalasPage implements OnInit {

  id: any;
  posicion: number;
  peliculas: any = [];
  nombre: string;
  resena: string;

  constructor(
    private activatedRoute: ActivatedRoute,
    private http: HttpClient
  ) { }

  ngOnInit() {
    this.id = this.activatedRoute.snapshot.paramMap.get("id");
    this.posicion = this.id - 1;
    console.log("id", this.id)


    this.getPeliculas().subscribe(res=>{
      console.log("Res",res)
      this.peliculas = res;
      this.nombre = this.peliculas[this.posicion].nombre;
      this.resena = this.peliculas[this.posicion].resena;
      console.log("Name",this.nombre)

    });
  }

  getPeliculas(){
    return this.http
    .get("assets/files/peliculas.json")
    .pipe(
      map((res:any) =>{
        return res.data;
      })
    )
}

}
