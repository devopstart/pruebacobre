import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { FingerprintAIO } from '@ionic-native/fingerprint-aio/ngx';

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
export class LoginPage implements OnInit {

  token = 'df23653fgd7263dda'
  touch = '9032Desblock'
  constructor(
    private router: Router,
    private faio: FingerprintAIO

  ) { }

  ngOnInit() {
  }

  login(){
    localStorage.setItem('token',this.token)
    this.router.navigate(["/home"])
  }
  
  public showFingerprintAuthDlg() {

    this.faio.isAvailable().then((result: any) => {
      console.log(result)

      this.faio.show({
        cancelButtonTitle: 'Cancel',
        description: "Some biometric description",
        disableBackup: true,
        title: 'Scanner Title',
        fallbackButtonTitle: 'FB Back Button',
        subtitle: 'This SubTitle'
      })
        .then((result: any) => {
          console.log(result)
          alert("Successfully Authenticated!")
        })
        .catch((error: any) => {
          console.log(error)
          alert("Match not found!")
        });

    })
      .catch((error: any) => {
        console.log(error)
      });
  }


}
